@component('mail::message')
# Neue Fälle bekannt 
## Bereitschaften

Es liegen neue Zahlen in Verbindung mit der COVID-19 Pandemie in den Regensburger Bereitschaften vor:

@component('mail::table')
| Bezeichnung | Anzahl |
| :------------- |:-------------:|
| An COVID-19 erkrankte Helfer  | {{ $report->getNumberOfInfectedStaff() }} |
| Helfer in behördlich angeordneter Quarantäne | {{ $report->getNumberOfQuarantienedStaff() }} |
@endcomponent

## Weitere Meldungen
@component('mail::panel')
@if($report->getAdditionalEventReporting())
{{ $report->getAdditionalEventReporting() }}
@else
Keine zusätzlichen Meldungen bekannt
@endif
@endcomponent

Vielen Dank und bis zum nächsten Update!<br><br>
{{ config('app.name') }}
@endcomponent
