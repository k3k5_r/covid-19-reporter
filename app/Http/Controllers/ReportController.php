<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Recipient;
use App\Mail\ReportDefault;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $report = new Report();
        $report->numberOfInfectedStaff = $request->numberOfInfectedStaff;
        $report->numberOfQuarantienedStaff = $request->numberOfQuarantienedStaff;
        $report->additionalEventReporting = $request->additionalEventReporting;

        $report->save();

        return view('thanks');
    }


    /**
     * Holds functionality for sending E-Mails with scheduled run.
     * Sends only new values.
     * 
     * @param \App\Models\Report  $report
     * @return boolean
     */
    public function sendReport() {
        $report = Report::find(1);
        $recipients = Recipient::all();
        foreach ($recipients as $recipient) {
            Mail::to($recipient->email)->send(new ReportDefault($report));
        }

        DB::table('reports')->update(['alreadySent' => 1]);
        return true;
    }
}
