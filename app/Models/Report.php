<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class Report extends Model
{
    use HasFactory;

    protected $table = 'reports';

    protected $fillable = [
        'numberOfInfectedStaff', 'numberOfQuarantienedStaff', 'additionalEventReporting', 'alreadySent'
    ];

    public function getNumberOfInfectedStaff() {
        return DB::table('reports')->where('alreadySent', 0)->sum('numberOfInfectedStaff');
    }

    public function getNumberOfQuarantienedStaff() {
        return DB::table('reports')->where('alreadySent', 0)->sum('numberOfQuarantienedStaff');
    }

    public function getAdditionalEventReporting() {
        return $this->additionalEventReporting;
    }
}
