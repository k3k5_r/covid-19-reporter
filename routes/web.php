<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ReportController@index')->name('report.index');
Route::post('/report', 'ReportController@store')->name('report.store');

Route::get('mailable', function () {
    $report = App\Models\Report::find(1);

    return new App\Mail\ReportDefault($report);
});

Route::get('send', 'ReportController@sendReport');
Route::get('/impressum', function() {
    return view('impressum');
})->name('impressum');
Route::get('/datenschutz', function() {
    return view('datenschutz');
})->name('datenschutz');